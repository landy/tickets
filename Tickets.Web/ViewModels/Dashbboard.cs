﻿using System.Collections.Generic;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.ViewModels
{
    public class Dashbboard
    {
        public IEnumerable<Ticket> Tickets { get; set; }
        public IEnumerable<Column> Columns { get; set; }
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Priority> Priorities { get; set; }
    }
}