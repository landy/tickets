﻿namespace Tickets.Web.ViewModels.Dashboard
{
    public class Priority
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}