﻿namespace Tickets.Web.ViewModels.Dashboard
{
    public class Column
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}