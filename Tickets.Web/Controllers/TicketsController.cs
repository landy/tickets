﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tickets.Web.Models;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Controllers
{
    public class TicketsController : ApiController
    {
        private readonly TicketRepository ticketRepository;

        public TicketsController(TicketRepository ticketRepository)
        {
            this.ticketRepository = ticketRepository;
        }

        public HttpResponseMessage Post(Ticket ticket)
        {
            ticketRepository.Add(ticket);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, ticket);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = ticket.Id }));
            return response;
        }
    }
}
