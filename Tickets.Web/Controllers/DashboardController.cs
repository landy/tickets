﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tickets.Web.Models;
using Tickets.Web.ViewModels;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Controllers
{
    public class DashboardController : ApiController
    {
        private readonly TicketRepository ticketRepository = new TicketRepository();
        private readonly ColumnRepository columnRepository = new ColumnRepository();
        private readonly PriorityRepository priorityRepository = new PriorityRepository();
        private readonly UserRepository userRepository = new UserRepository();

        public Dashbboard Get()
        {
            return new Dashbboard()
                {
                    Tickets = ticketRepository.GetTickets(),
                    Columns = columnRepository.GetColumns(),
                    Priorities = priorityRepository.GetPriorities(),
                    Users = userRepository.GetUsers()

                };
        }
    }
}
