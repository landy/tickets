﻿'use strict';

var tickets = angular.module('tickets', []);

tickets.config(function ($routeProvider) {
    $routeProvider.
      otherwise({ controller: 'DashboardCtrl', templateUrl: 'Scripts/list.html' });
});

tickets.controller('DashboardCtrl', function DashboardCtrl($scope, storage) {
    storage.get(function(data) {
        $scope.tickets = data.Tickets;
        $scope.columns = data.Columns;
        $scope.priorities = data.Priorities;
        $scope.users = data.Users;
    });

    $scope.getTicketsForPriority = function (column, priority) {
        console.log("getTickets");
        var selectedTickets = new Array();
        for (var index in $scope.tickets) { 
            var ticket = $scope.tickets[index];
            if (ticket.ColumnId == column.Id && ticket.PriorityId == priority.Id) {
                selectedTickets.push(ticket);
            }
        }
        return selectedTickets;
    };

    $scope.getVisiblePriorities = function(column) {
        var visiblePriorities = new Array();
        for (var index in $scope.priorities) {
            var priority = $scope.priorities[index];
            if ($scope.getTicketsForPriority(column, priority).length) {
                visiblePriorities.push(priority);
            }
        }
        return visiblePriorities;
    };
});

tickets.factory('storage', function ($http) {
    return {
        get: function (cb) {
            $http.get("/api/dashboard").success(function (data) {
                cb(data);
            });
        }
    };
});
