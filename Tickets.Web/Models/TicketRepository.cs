﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Models
{
    public class TicketRepository
    {
        private static List<Ticket> tickets = new List<Ticket>()
                {
                    new Ticket()
                        {
                            Id = 1,
                            Title = "První ticket",
                            PriorityId = 1,
                            AssignedUserId = 1,
                            Deadline = new DateTime(2012, 12, 24),
                            ColumnId = 1
                        },
                    new Ticket()
                        {
                            Id = 2,
                            Title = "Druhý ticket",
                            PriorityId = 1,
                            AssignedUserId = 1,
                            Deadline = new DateTime(2012, 12, 23),
                            ColumnId = 2
                        },
                    new Ticket()
                        {
                            Id = 3,
                            Title = "Třetí ticket",
                            PriorityId = 2,
                            AssignedUserId = 2,
                            Deadline = new DateTime(2012, 11, 23),
                            ColumnId = 1
                        }
                };


        public IEnumerable<Ticket> GetTickets()
        {
            return tickets;
        }

        public void Add(Ticket ticket)
        {
            var maxId = tickets.Max(x => x.Id);
            ticket.Id = ++maxId;
            tickets.Add(ticket);
        }
    }
}