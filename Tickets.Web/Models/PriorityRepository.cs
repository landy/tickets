﻿using System.Collections.Generic;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Models
{
    public class PriorityRepository
    {
        public IEnumerable<Priority> GetPriorities()
        {
            return new List<Priority>()
                {
                    new Priority(){Id = 1, Name = "Normální"},
                    new Priority(){Id = 2, Name = "Nízká"},
                    new Priority(){Id = 3, Name = "Vysoká"}
                };
        }
    }
}