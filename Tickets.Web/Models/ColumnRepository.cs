﻿using System.Collections.Generic;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Models
{
    public class ColumnRepository
    {
        public IEnumerable<Column> GetColumns()
        {
            return new List<Column>()
                {
                    new Column(){Id = 1, Name = "Backlog"}, 
                    new Column(){Id = 2, Name = "In progress"}, 
                    new Column(){Id = 3, Name = "Done"}
                };
        }
    }
}