﻿using System.Collections.Generic;
using Tickets.Web.ViewModels.Dashboard;

namespace Tickets.Web.Models
{
    public class UserRepository
    {
        public IEnumerable<User> GetUsers()
        {
            return new List<User>()
                {
                    new User(){Id = 1, Name = "Jirka"},
                    new User(){Id = 2, Name = "Honza"},
                    new User(){Id = 3, Name = "Jára"},
                };
        }
    }
}